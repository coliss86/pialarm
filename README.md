# Pi Alarm

![Wiring](doc/alarm.jpg)

## Components
 * Raspberry Pi Zero
 * [Pimoroni pHAT DAC ](https://shop.pimoroni.com/products/phat-dac)
 * [Pimoroni Touch pHAT](https://shop.pimoroni.com/products/touch-phat)
 * DS1307 RTC Clock
 * TM1637 4\*7-segments displays
 * Optional : serial adapter CP2102

## Key bindings
In default mode where the clock is displayed : 
 * `D` : cycle brightness
 * 3\*`Back` : enter edit mode to setup alarm

In edit mode
 * `A` : -1 hour
 * `B` : +1 hour
 * `C` : -1 minute
 * `D` : +1 minute
 * `Enter` : back to clock mode

When ringing
 * `Enter` : switch off alarm
 * `Back` : snooze

## Systemd service
```
sudo cp pialarm.service  /etc/systemd/system/
sudo systemctl enable pialarm.service
sudo systemctl start pialarm.service
```

## Links
 * [TM1637 lib](https://github.com/depklyon/raspberrypi-python-tm1637) with [patch from me](https://gitlab.com/coliss86/raspberrypi-python-tm1637)

