#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import signal
import sys
import os
from time import sleep

from wrapper import Synchronized
from observable import Observable
from applications.clock import Clock
from applications.editor import Editor
from applications.alarm import Alarm
from applications.ticktock import TickTock
from display.tm1637 import TM1637
from buttons.touchhat import Buttons

os.environ['DEBUG'] = str(len(sys.argv) > 1 and sys.argv[1] == '-d')

# stop handler
def stop_handler(signal, frame):
    display.clear()
    sys.exit(0)

signal.signal(signal.SIGINT, stop_handler)
signal.signal(signal.SIGTERM, stop_handler)


# dependency injection

# Initialize the clock (GND, VCC=3.3V, DIO=8, CLK=7)
display = Synchronized(TM1637(clk=7, dio=8, brightness=2))
buttons = Buttons()
buttons.init()

alarm = Alarm(display)
clock = Clock(display)
editor = Editor(display)

ticktock = TickTock()

Observable().notify("app-start", "value")

while (True):
    sleep(10)


