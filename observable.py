from datetime import datetime
import operator
import os

# This is a singleton because the touchphat module add decorator which can only be executed on static method, or 
# method without any context, thus to publish an event, it has to be shared by all python modules...
class Observable:
    class __Observable:
        def __init__(self):
            self.__observers = []

        def __str__(self):
            return 'observable ' + self.__observers

        def notify(self, id, value):
            if os.environ['DEBUG'] == 'True' and id != 'time' and id != 'blink':
                print(datetime.strftime(datetime.now(), '%H:%M:%S'), "-", id, "=", str(value))
            post = {"id": id, "value": value}
            for observer in self.__observers:
                observer.update(post)

        def register_observer(self, observer):
            self.__observers.append(observer) 

    instance = None

    def __new__(cls): # __new__ always a classmethod
        if not Observable.instance:
            Observable.instance = Observable.__Observable()
        return Observable.instance
    
