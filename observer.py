class Observer:
    __event_function = {}

    def __init__(self, event_function):
        self.__event_function = event_function

    def update(self, event):
        if event['id'] in self.__event_function:
            func = self.__event_function[event['id']]
            getattr(self, func)(event["value"])

