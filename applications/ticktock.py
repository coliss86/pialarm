import signal
from datetime import datetime

from observable import Observable

class TickTock:
    _tick = False

    def __init__(self):
        signal.signal(signal.SIGALRM, self.tick)
        signal.setitimer(signal.ITIMER_REAL, 0.5, 0.5)

    def tick(self, signal, frame):
        if self._tick:
            Observable().notify("time", datetime.now())

        Observable().notify("blink", "")
        self._tick = not self._tick

