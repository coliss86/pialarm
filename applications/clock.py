import signal
import time

from observable import Observable
from observer import Observer

class Clock (Observer):
    _display = None
    _blink = True
    _display_active = True
    _time = None

    def __init__(self, display):
        Observer.__init__(self, {
            'time': 'show_time',
            'blink': 'blink',
            'state': 'activate_display'
        })
        self._display = display
        Observable().register_observer(self)

    def activate_display(self, value):
        self._display_active = ( value == "edit-end" or value == "app-start" )
        self.show_time(self._time)

    def show_time(self, time):
        self._time = time
        if self._display_active:
            self._display.numbers(time.hour, time.minute, True)
    
    def blink(self, _):
        if self._display_active and self._blink and self._time:
                self._display.numbers(self._time.hour, self._time.minute, False)
        self._blink = not self._blink

