from datetime import datetime
import configparser
from pathlib import Path
import pygame.mixer
import glob
import random
from threading import Timer

from observable import Observable
from observer import Observer

class Alarm (Observer):
    _display = None
    _screen_active = True
    _alarm = None
    _last_ring = None
    _config = None
    _config_alarms = None
    _soundfiles = None

    def __init__(self, display):
        self._display = display
        self._config = ConfigAlarm()
        self._config_alarms = self._config.getAlarms()
        self._alarm = datetime.strptime(self._config_alarms['alarm_1'], '%H:%M')
        self._soundfiles = glob.glob(self._config_alarms['music_folder'] + '/**/*.mp3')
        pygame.mixer.init()
        Observer.__init__(self, {
            'time': 'check_alarm',
            'state': 'handle_alarm',
            'buttons': 'buttons',
            'alarm-edited': 'save_alarm'
        })
        Observable().register_observer(self)

    def handle_alarm(self, value):
        if value == "edit-start":
            Observable().notify("alarm-value", self._alarm)

        self._screen_active = ( value == "edit-end" )

    def check_alarm(self, time):
        if (self._screen_active
            and self.same_time(time, self._alarm)
            and not self.has_already_ringing(time)):
            self._last_ring = time
            self.dring()

    def save_alarm(self, alarm):
        self._alarm = alarm
        self._config_alarms['alarm_1'] = datetime.strftime(alarm, '%H:%M')
        self._config.save()

    def same_time(self, time1, time2):
        return (time1.hour == time2.hour 
                and time1.minute == time2.minute)

    def has_already_ringing(self, time):
        return (self._last_ring != None 
                and self.same_time(time, self._last_ring)
                and time.day == self._last_ring.day)

    def dring(self):
        Observable().notify("alarm", "dring")
        choosen = []
        val = self._config_alarms['number_of_song_to_play']
        nb = int(val) if val else 3
        while (len(choosen) < nb): 
            music = random.choice(self._soundfiles)
            if music not in choosen:
                choosen.append(music)
        Observable().notify("play", choosen[0])
        pygame.mixer.music.load(choosen[0])
        pygame.mixer.music.play(1)
        for i in range(1, len(choosen)):
            pygame.mixer.music.queue(choosen[i])

    def is_music_playing(self):
        return pygame.mixer.music.get_busy()

    def switch_off(self):
        if self.is_music_playing():
            Observable().notify("alarm", "stop")
            pygame.mixer.music.stop()

    def play_again(self):
        Observable().notify("alarm", "play_again")
        self.dring()

    def snooze(self):
        if self.is_music_playing():
            self.switch_off()
            val = self._config_alarms['snooze_time_sec']
            Observable().notify("alarm", "snooze %s sec" % val)
            snooze_time_sec = int(val) if val else 600
            t = Timer(snooze_time_sec, self.play_again)
            t.start()

    def buttons(self, value):
        if self._screen_active and value in self.button_action:
            func = self.button_action[value]
            func(self)

    button_action = {
        'Enter': switch_off,
        'Back': snooze
    }

class ConfigAlarm:
    _path = None
    _config = None

    def __init__(self):
        self._path = '%s/.config/pialarm.cfg' % str(Path.home())
        self._config = configparser.ConfigParser()
        self._config.read(self._path)
        Observable().notify("alarm", "read %s/.config/pialarm.cfg" % str(Path.home()))

    def get(self):
        return self._config

    def getAlarms(self):
        return self._config['alarms']

    def save(self):
        with open(self._path, 'w') as configfile:
            self._config.write(configfile)

