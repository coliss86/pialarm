from time import sleep, clock

from observable import Observable
from observer import Observer

class Editor (Observer):
    _cpt_edit = 0
    _alarm = None
    _edit = False
    _hour_edit = True
    _blink = False
    _brightness = 0.3
    _display = None
    _blink = False
    _last_press_edit = None

    def __init__(self, display):
        Observer.__init__(self, {
            'buttons': 'buttons',
            'blink': 'blink',
            'alarm-value': 'set_alarm'
        })
        self._display = display
        Observable().register_observer(self)

    def enter_edit_mode(self):
        if self._edit:
            return
        self._cpt_edit += 1
        if self._last_press_edit != None:
            diff = clock() - self._last_press_edit
            if diff > 0.2:
                self.reset_edit_mode(1)
        self._last_press_edit = clock()
        if self._cpt_edit == 3:
            Observable().notify("state", "edit-start")
            self._hour_edit = True
            self._edit = True
            self._last_press_edit = None

    def reset_edit_mode(self, cpt = 0):
        self._cpt_edit = cpt
        self._last_press_edit = None
        self._alarm = None
        self._edit = False

    def exit_edit_mode(self):
        if self._edit:
            self._display.clear()
            Observable().notify("alarm-edited", self._alarm)
            Observable().notify("state", "edit-end")
            self.reset_edit_mode()

    def set_alarm(self, value):
        self._alarm = value;
        self.show_alarm()

    def increment_hours(self):
        if self._edit:
            self._blink = False
            hour = self._alarm.hour + 1
            if hour == 24: hour = 0
            self._alarm = self._alarm.replace(hour=hour)
            self.show_alarm()

    def increment_minutes(self):
        if self._edit:
            self._blink = False
            minute=self._alarm.minute + 1
            if minute == 60: minute = 0
            self._alarm = self._alarm.replace(minute=minute)
            self.show_alarm()

    def decrement_hours(self):
        if self._edit:
            self._blink = False
            hour = self._alarm.hour - 1
            if hour == -1: hour = 23
            self._alarm = self._alarm.replace(hour=hour)
            self.show_alarm()

    def decrement_minutes(self):
        if self._edit:
            self._blink = False
            minute=self._alarm.minute - 1
            if minute == -1: minute = 59
            self._alarm = self._alarm.replace(minute=minute)
            self.show_alarm()

    def show_alarm(self):
        self._display.numbers(self._alarm.hour, self._alarm.minute, True)

    def cycle_brightness(self):
        if not self._edit:
            self._brightness -= 1
            if (self._brightness < 0): self._brightness = 3
            self._display.brightness(self._brightness)

    def blink(self, _):
        if self._edit:
            if self._alarm == None:
                self._display.clear()
                return
            self.show_alarm()
            if self._blink:
                self._display.write([0,0], 0)
                self._display.write([0,0], 2)
            self._blink = not self._blink

    def buttons(self, value):
        if value in self.button_action:
            func = self.button_action[value]
            if callable(func):
                func(self)
            else:
                for fun in func: fun(self)


    button_action = {
        'Back': enter_edit_mode,
        'A': decrement_hours,
        'B': increment_hours,
        'C': decrement_minutes,
        'D': [increment_minutes, cycle_brightness],
        'Enter': exit_edit_mode
    }

