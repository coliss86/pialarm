import time

class Console():
    cpt = 0
    active = True
    filtre = False

    def clear(self):
        pass

    def show_doublepoint(self, on):
        pass

    def display_time(self, time):
        if self.active:
            if self.filtre:
                self.cpt +=1
                if self.cpt == 10:
                    self._display(time)
                    self.cpt = 0
            else:
                self._display(time)

    def set_brightness(self, brightness):
        pass

    def _display(self, t, hour24=True):
        if isinstance(t, time.struct_time):
            hour = t.tm_hour
            minute = t.tm_min
        else:
            hour = t.hour
            minute = t.minute
        if not hour24:
            hour = 12 if (hour % 12) == 0 else hour % 12
        d0 = hour // 10 if hour // 10 else 0
        d1 = hour % 10
        d2 = minute // 10
        d3 = minute % 10
        print("{0}{1}:{2}{3}".format(d0, d1, d2, d3)) 

    def cleanup(self):
        pass
