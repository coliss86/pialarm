import signal
import time
import touchphat

from observable import Observable

# Available buttons : Back, A, B, C, D, Enter
@touchphat.on_release(['Back','A','B','C','D','Enter'])
def handle_touch(event):
    Observable().notify("buttons", event.name)

class Buttons:

    def init(self):
        touchphat.all_on()
        time.sleep(0.1)
        touchphat.all_off()
        time.sleep(0.1)
        touchphat.all_on()
        time.sleep(0.1)
        touchphat.all_off()

