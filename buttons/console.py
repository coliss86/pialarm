from time import sleep

from observable import Observable

class Buttons():
    def press(self, button):
        Observable().notify("buttons", button)

