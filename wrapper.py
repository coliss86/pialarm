from threading import RLock
from display.tm1637 import TM1637

class Synchronized:
    """Wrapper that synchronized calls to the underlying methods"""
    def __init__(self,wrapped_class):
        self._wrapped_class = wrapped_class
        self._lock = RLock()

    def __getattr__(self,attr):
        orig_attr = self._wrapped_class.__getattribute__(attr)
        if callable(orig_attr):
            def hooked(*args, **kwargs):
                self.enter()
                try:
                    return orig_attr(*args, **kwargs)
                finally:
                    self.exit()
            return hooked
        else:
            return orig_attr

    def enter(self):
        self._lock.acquire()

    def exit(self):
        self._lock.release()

